<?php

namespace App\Service;

interface PostalAddressApiServiceInterface
{
    public function getGeolocationFromPostalAddress(string $address): ?array;
}
