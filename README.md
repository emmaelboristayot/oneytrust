# Welcome to Oneytrust!

The goal of this topic is to explain you how to build and run this application and the most important, **how is it made**.
**As you are lucky**, there are only few commands to deal with this project.

# Technical documentation

 1. Installation
 2. Useful command
 3. Architecture
 4. Testing and Continuous integration

## 1. Installation

#### Clone repository
    git clone git@gitlab.com:emmaelboristayot/oneytrust.git
#### Build 
    cd oneytrust
    make build
    make up

### Run

> ***IMPORTANT***: Please note that, the Api server takes 5 to 10 min to be fully functional. Before this time you can get ERROR 502 Bade
> gateway


Before run this application, check if all build's process are finished.
To enjoy with this wonderful web application, go to [http://localhost:8089](http://localhost:8089)

## 2. Useful command

To simplify things, i decided to use **Makefile** in this project. Because, this is very useful and it's now a common best practice. (For more informations go to [le makefile parfait pour symfony](https://www.strangebuzz.com/fr/snippets/le-makefile-parfait-pour-symfony))

### see all commands
    make
### run phpUnits Test
    make test
### run composer install
    make install

##  3. Architecture

This project is likely how most and most web application are built nowaday.
Below, this is how I organise the project folder.

> **/oneytrust**
> 	- **/front**
> 	- **/back**
> 	- docker-compose.yml
> 	- .gitlab-ci.yml

### Web Architecture
```mermaid
graph LR
A[Vuejs Front app] -- GET --> D[SF 4.4 REST API]
D -- Distance JSON --> A
D[SF 4.4 REST API] -- GET --> B[ipify API]
D[SF 4.4 REST API] -- GET --> C[geocode API]
```

#### Front-end
We have a front-end application that consume a web api. I chose to use Vuejs for this part because it's very easy to build quickly a web app.
The front-end server run in [localhost:8089](http://localhost:8089/)
#### Back-end
The api rest service is made with symfony 4.4.4 and php7.1. All business logics are in the unique service class AddressService.php
The back-end server run in [localhost:8000](http://localhost:8000/)

### Container Architecture

The docker-compose.yml have 3 containers specifications. 
**nginx_container** and **php_container** are separate each others to reduce complexity. But work together to provide a powerful Nginx server.

**node_container** is also a container with Nginx and nodejs. Node is used to build et deploy Vuejs app. The build folder is copy Nginx folder for expose that.


##  4. Testing and Continuous integration

It's a common best practice to test class service in symfony. therefore, i decided to test the class AddressService.php. I used this class to:
 - retrieve geolocation form Ip Address
 - retrieve geolocation form Postal Address
 - Calculate distance between two coordinates

### CI by gitlab
Thanks to gitlab because we can set CI with a free account. This is very great. 
To use it, I have juste add [gitlab-ci.yml](https://gitlab.com/emmaelboristayot/oneytrust/-/blob/develop/.gitlab-ci.yml) in the project's root. It's like a dockerfile, and you can chose what image you decide to run your project. I use php7.1 image for this project.
(For more informations go to [docs.gitlab.com](https://docs.gitlab.com/ee/ci/README.html))