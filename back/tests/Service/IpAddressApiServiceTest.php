<?php

namespace App\Tests\Service;

use App\Service\IpAddressApiService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class IpAddressApiServiceTest extends TestCase
{
    public function testGetGeolocationFromIpAddress()
    {

        $body = '{
            "ip": "8.8.8.8",
            "location": {
                "country": "US",
                "region": "California",
                "city": "Mountain View",
                "lat": 48.63333,
                "lng": 2.45,
                "postalCode": "94043",
                "timezone": "-07:00",
                "geonameId": 5375481
            }
        }';

        $response = [
            new MockResponse($body)
        ];

        $httpClient = new MockHttpClient($response);

        $ipAddressApiService = new IpAddressApiService($httpClient, ['ipAddressApiUrl' => 'https://test.com', 'apiKey' => '']);

        $coordonate = $ipAddressApiService->getGeolocationFromIpAddress('ip');

        $this->assertTrue(is_array($coordonate));
        $this->assertEquals($coordonate, ['lat' => 48.63333, 'lon' => 2.45]);
    }
}
