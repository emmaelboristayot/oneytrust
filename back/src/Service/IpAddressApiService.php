<?php

namespace App\Service;

use App\Service\IpAddressApiServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class IpAddressApiService implements IpAddressApiServiceInterface
{
    /** @var string */
    private $ipAddressApiUrl;

    /** @var string */
    private $apiKey;

    public function __construct(HttpClientInterface $httpClient, array $params)
    {
        $this->httpClient = $httpClient;
        $this->ipAddressApiUrl = $params['ipAddressApiUrl'];
        $this->apiKey = $params['apiKey'];
    }

    /**
     * getGeolocationFromIpAddress
     *
     * @param string $ip
     *
     * @return array
     */
    public function  getGeolocationFromIpAddress(string $ip): ?array
    {
        $response = $this->httpClient->request(Request::METHOD_GET, sprintf($this->ipAddressApiUrl, $this->apiKey, $ip));
        $content = json_decode($response->getContent(), true);

        return ['lat' => $content['location']['lat'], 'lon' => $content['location']['lng']];
    }

}