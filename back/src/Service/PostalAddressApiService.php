<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use App\Service\PostalAddressApiServiceInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class PostalAddressApiService implements PostalAddressApiServiceInterface
{

    /** @var string */
    private $postalAddressApiUrl;

    public function __construct(HttpClientInterface $httpClient, array $params)
    {
        $this->httpClient = $httpClient;
        $this->postalAddressApiUrl = $params['postalAddressApiUrl'];
    }

    /**
     * getGeolocationFromPostalAddress
     *
     * @param string $address
     *
     * @return array
     */
    public function  getGeolocationFromPostalAddress(string $address): ?array
    {
        $response = $this->httpClient->request(Request::METHOD_GET, sprintf($this->postalAddressApiUrl, \urlencode($address)));
        $content = json_decode( $response->getContent(), true);

        return isset($content['latt']) && isset($content['longt'])
            ? ['lat' => (string)$content['latt'], 'lon' => (string)$content['longt']]
            : null;
    }

}