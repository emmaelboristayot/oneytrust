<?php

namespace App\Tests\Service;

use App\Service\AddressService;
use PHPUnit\Framework\TestCase;
use App\Service\IpAddressApiService;
use App\Service\PostalAddressApiService;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class AddressServiceTest extends TestCase
{

    /** @var PostalAddressApiService */
    private $postalAddressApi;

    /** @var IpAddressApiService */
    private $ipAddressApiService;

    /** @var AddressService */
    private $addressService;

    public function setUp()
    {
        $this->postalAddressApiService = $this->createMock(PostalAddressApiService::class);
        $this->ipAddressApiService = $this->createMock(IpAddressApiService::class);
        $this->addressService = new AddressService($this->postalAddressApiService, $this->ipAddressApiService);
    }

    public function testGetGeolocationFromIpAddress()
    {
        // Configurer le bouchon.
        $this->ipAddressApiService->method('getGeolocationFromIpAddress')
             ->willReturn([]);
        $coordonate = $this->addressService->getGeolocationFromIpAddress('ip');

        $this->assertTrue(is_array($coordonate));
    }

    public function testGetGeolocationFromPostalAddress()
    {
        // Configurer le bouchon.
        $this->postalAddressApiService->method('getGeolocationFromPostalAddress')
             ->willReturn([]);
        $coordonate = $this->addressService->getGeolocationFromPostalAddress('address');

        $this->assertTrue(is_array($coordonate));
    }

    public function testGetDistanceBetweenTwoCoordonate()
    {
        $distance = $this->addressService->getDistanceBetweenTwoCoordonate(['lat' => 48.63333, 'lon' => 2.45], ['lat' => 48.54319, 'lon' => 2.64025]);

        $this->assertTrue(is_string($distance));
        $this->assertEquals($distance, '17.21172373943');
    }
}
