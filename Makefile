# Setup ————————————————————————————————————————————————————————————————————————
DOCKER        	= docker-compose
DOCKER_EXEC   	= docker exec -it
COMPOSER      	= $(EXEC_PHP) composer
CONTAINER_NAME  = php_container

## —— 🐝 Symfony Makefile 🐝 ———————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

wait: ## Sleep 5 seconds
	sleep 5

## —— Docker 🐳 ————————————————————————————————————————————————————————————————

build: docker-compose.yml ## Build the docker 
	$(DOCKER) -f docker-compose.yml build

up: docker-compose.yml ## Start the docker 
	$(DOCKER) -f docker-compose.yml up -d

down: docker-compose.yml ## Stop the docker hub
	$(DOCKER) down --remove-orphans

## —— Composer 🧙‍♂️ ————————————————————————————————————————————————————————————
install: ./back/composer.lock ## Install vendors according to the current composer.lock file
	$(DOCKER_EXEC) $(CONTAINER_NAME) $(COMPOSER) install

update: ./back/composer.json ## Update vendors according to the composer.json file
	$(DOCKER_EXEC) $(CONTAINER_NAME) $(COMPOSER) update

## —— Tests ✅ —————————————————————————————————————————————————————————————————
test: ./back/phpunit.xml.dist ## Launch unit tests
	$(DOCKER_EXEC) $(CONTAINER_NAME) ./bin/phpunit