<?php

namespace App\Controller;

use App\Service\AddressService;
use App\Form\CoordonatesDistanceType;
use Symfony\Component\HttpFoundation\{Request, Response, JsonResponse};
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddressController extends AbstractController
{

    /**
     * @Route("/calculate-distance", name="calculate_distance")
     */
    public function index(Request $request, AddressService $addressService)
    {
        $form = $this->createForm(CoordonatesDistanceType::class);

        $form->submit(json_decode($request->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) {

            $coordonateFromIp = $addressService->getGeolocationFromIpAddress($form['ip_address']->getData());

            $coordonateFromAddress = $addressService->getGeolocationFromPostalAddress($form['postal_address']->getData());
    
            $distance = $addressService->getDistanceBetweenTwoCoordonate($coordonateFromIp, $coordonateFromAddress);

            return $this->json(['distance' => $distance]);
        }

        return $this->json($form->getErrors(true));
    }

}
