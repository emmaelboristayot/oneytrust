<?php

namespace App\Service;

interface IpAddressApiServiceInterface
{
    public function  getGeolocationFromIpAddress(string $ip): ?array;
}
