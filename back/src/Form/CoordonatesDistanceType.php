<?php

namespace App\Form;

use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\{NotBlank, Regex};
use Symfony\Component\Form\Extension\Core\Type\TextType;


class CoordonatesDistanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ip_address', TextType::class, [
                'constraints' => [
                    new Regex([
                        'pattern'   => '/^(?=.*[^\.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/',
                        'match'     => true,
                        'message'   => 'IP address is not correct.'
                    ]),
                    new NotBlank(['message' => 'Empty field is not allowed'])
                ],
            ])
            ->add('postal_address', TextType::class, [
                'constraints' => [
                    new Regex([
                        'pattern'   => '/\d{1,3}.?\d{0,3}\s[a-zA-Z]{2,30}\s[a-zA-Z]{2,15}/',
                        'match'     => true,
                        'message'   => 'Postal address is not correct.'
                    ]),
                    new NotBlank(['message' => 'Empty field is not allowed'])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
