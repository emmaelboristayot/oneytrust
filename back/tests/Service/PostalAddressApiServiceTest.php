<?php

namespace App\Tests\Service;

use App\Service\PostalAddressApiService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class PostalAddressApiServiceTest extends TestCase
{
    public function testGetGeolocationFromPostalAddress()
    {

        $body = '{
            "standard": {
            "stnumber": "407",
            "addresst": "avenue de la libération",
            "postal": "77350",
            "region": "FR",
            "prov": "FR",
            "city": "Le Mée-sur-Seine",
            "countryname": "France",
            "confidence": "0.8"
            },
            "longt": "2.64025",
            "alt": {},
            "elevation": {},
            "latt": "48.54319"
            }';

        $response = [
            new MockResponse($body)
        ];

        $httpClient = new MockHttpClient($response);

        $postalAddressApiService = new PostalAddressApiService($httpClient, ['postalAddressApiUrl' => 'https://test.com']);

        $coordonate = $postalAddressApiService->getGeolocationFromPostalAddress('address');

        $this->assertTrue(is_array($coordonate));
        $this->assertEquals($coordonate, ['lat' => "48.54319", 'lon' => "2.64025"]);
    }
}
