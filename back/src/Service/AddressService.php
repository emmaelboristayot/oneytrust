<?php

namespace App\Service;

use App\Service\{ IpAddressApiService, PostalAddressApiService, IpAddressApiServiceInterface, PostalAddressApiServiceInterface };
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AddressService
{

    /** @var PostalAddressApiService */
    private $postalAddressApi;

    /** @var IpAddressApiService */
    private $ipAddressApi;

    public function __construct(PostalAddressApiServiceInterface $postalAddressApi, IpAddressApiServiceInterface $ipAddressApi)
    {
        $this->postalAddressApi = $postalAddressApi;
        $this->ipAddressApi = $ipAddressApi;
    }

    /**
     * getGeolocationFromIpAddress
     *
     * @param string $ip
     *
     * @return array
     */
    public function getGeolocationFromIpAddress(string $ip): ?array
    {
        return $this->ipAddressApi->getGeolocationFromIpAddress($ip);
    }

    /**
     * getGeolocationFromPostalAddress
     *
     * @param string $address
     *
     * @return array
     */
    public function getGeolocationFromPostalAddress(string $address): ?array
    {
        return $this->postalAddressApi->getGeolocationFromPostalAddress($address);
    }

    /**
     * getDistanceBetweenTwoCoordonate
     *
     * @param array $coordonates
     *
     * @return string
     */
    public function getDistanceBetweenTwoCoordonate(array ...$coordonates): ?string
    {
        $lon1 = $coordonates[0]['lon'];
        $lon2 = $coordonates[1]['lon'];
        $lat1 = $coordonates[0]['lat'];
        $lat2 = $coordonates[1]['lat'];
            
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515 * 1.609344;

            return $miles;
        }
    }
}
